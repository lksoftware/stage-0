const path = require('path');
const fs = require('fs');
const url = require('url');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const envPublicUrl = process.env.PUBLIC_URL;

function ensureSlash(path, needsSlash) {
  const hasSlash = path.endsWith('/');
  if (hasSlash && !needsSlash) {
    return path.substr(path, path.length - 1);
  } else if (!hasSlash && needsSlash) {
    return `${path}/`;
  } else {
    return path;
  }
}

const getPublicUrl = appPackageJson =>
  envPublicUrl || require(appPackageJson).homepage;

function getServedPath(appPackageJson) {
  const publicUrl = getPublicUrl(appPackageJson);
  const servedUrl =
    envPublicUrl || (publicUrl ? url.parse(publicUrl).pathname : '/');
  return ensureSlash(servedUrl, true);
}

module.exports = {
  web: {
    appBuild: resolveApp('build/web'),
    appPublic: resolveApp('static/web-public'),
    appHtml: resolveApp('static/web-public/index.html'),
    appIndexJs: resolveApp('src/web-tool/index.js'),
    appSrc: resolveApp('src/web-tool'),
    appPackageJson: resolveApp('src/web-tool/package.json'),
    publicUrl: getPublicUrl(resolveApp('src/web-tool/package.json')),
    servedPath: getServedPath(resolveApp('src/web-tool/package.json'))
  },
  dotenv: resolveApp('.env'),
  appPackageJson: resolveApp('package.json'),
  appNodeModules: resolveApp('node_modules'),
  testsSetup: resolveApp('src/setupTests.js')
};
