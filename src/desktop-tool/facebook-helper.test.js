import Facebook from './facebook-helper';

describe('test facebook helper', () => {
  const facebook = new Facebook('nguyenhoangvi000@gmail.com', 'Vihoang@123');
  describe('test not logined function', () => {
    it("homepage's title", async () => {
      await facebook.get('/');
      expect(facebook.getTitle()).toBe('Facebook - Đăng nhập hoặc đăng ký');
    });
    it("loginpage's title", async () => {
      await facebook.get('/login.php');
      expect(facebook.getTitle()).toBe('Đăng nhập Facebook | Facebook');
    });
  });

  describe('test logined function', () => {
    it("homepage's title", async () => {
      await facebook.login();

      await facebook.get('/');
      expect(facebook.getTitle()).toBe('Facebook');
    });

    it("loginpage's title", async () => {
      await facebook.login();

      await facebook.get('/');
      expect(facebook.getTitle()).toBe('Facebook');
    });
  });
});
