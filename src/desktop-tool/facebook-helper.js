import Nightmare from 'nightmare';
import electron from 'electron';
import { createQueue } from 'seq-queue';
const userAgent =
  'Opera/12.02 (Android 4.1; Linux; Opera Mobi/ADR-1111101157; U; en-US) Presto/2.9.201 Version/12.02';
const fbUrl = url => `https://m.facebook.com${url}`;

class Facebook {
  constructor(username, password) {
    this._username = username;
    this._password = password;
    console.log(electron.app.getPath('exe'));
    this._browser = new Nightmare({
      electronPath: electron.app.getPath('exe'),
      show: true,
      webPreferences: {
        partition: 'persist:facebook.com'
      }
    });
    this._browser.useragent(userAgent);
    this._task = createQueue(10000);
  }

  login = () => {
    return new Promise(resolve => {
      this._task.push(task => {
        this._browser
          .goto(fbUrl('/'))
          .evaluate(() => document.title)
          .then(title => {
            resolve(title === 'Facebook');
            task.done();
          })
          .catch(err => {
            console.log(err);
          });
      });
    });
  };
}

export default Facebook;
