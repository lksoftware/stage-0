import { app, BrowserWindow } from 'electron';
import Facebook from './facebook-helper';

let mainWindow;
function createWindow() {
  mainWindow = new BrowserWindow({
    width: 400,
    height: 700,
    show: false,
    resizable: false,
    center: true,
    title: 'Vietface v0.1.0'
  });
  mainWindow.setMenu(null);
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
  mainWindow.show();

  const facebook = new Facebook('nguyenhoangvi000@gmail.com', 'Vihoang@123');

  console.log('start');
  facebook
    .login()
    .then(result => {
      console.log(result);
    })
    .catch(error => {
      console.log(error);
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
