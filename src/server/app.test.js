import request from 'supertest';
import app from './app';

describe('Test the root path', () => {
  test('It should response the GET method as text/html', () => {
    return request(app)
      .get('/')
      .expect('Content-Type', /text\/html/)
      .expect(200);
  });
  test('It should response the index.html for all unknown request', () => {
    return request(app)
      .get('/randomizeurl')
      .expect('Content-Type', /text\/html/)
      .expect(200);
  });
});
