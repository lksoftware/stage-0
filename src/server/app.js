import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import logger from 'morgan';
import errorHandler from 'errorhandler';
import path from 'path';
import expressStatusMonitor from 'express-status-monitor';

import api from './api';

const app = express();

app.use(expressStatusMonitor());
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', api);

const staticResourceDir = path.resolve('build/web');
app.use(express.static(staticResourceDir));
app.get('/', (req, res) => {
  res.sendFile(staticResourceDir + './index.html');
});
app.use(errorHandler());

export default app;
