import chalk from 'chalk';
import ngrok from 'ngrok';

import app from './app';
console.log(chalk.black.bgGreen('Starting Vietface Server....'));

const PORT = process.env.PORT || 8080;
app.listen(PORT, error => {
  if (error) {
    console.log(`${chalk.red('X')} Can\'t not listen on port ${PORT}`);
    throw error;
  }
  if (process.env.NGROK === true) {
    ngrok.connect(PORT, (error, result) => {
      if (error) {
        console.log(`${chalk.red('X')} Can\'t not start ngrok on port ${PORT}`);
        throw error;
      }
      console.log(result);
    });
  }
  console.log(`${chalk.green('V')} Vietface Server listening on port ${PORT}`);
});
