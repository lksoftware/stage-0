export default {
  Users: [
    {
      id: 0,
      email: 'job@d.com',
      profile: {
        displayName: 'job'
      }
    },
    {
      id: 1,
      email: 'alice@d.com',
      profile: {
        displayName: 'alice'
      }
    },
    {
      id: 2,
      email: 'jack@d.com',
      profile: {
        displayName: 'jack'
      }
    }
  ],
  Posts: [
    {
      id: 0,
      title: 'post a',
      author: 0
    },
    {
      id: 1,
      title: 'post b',
      author: 0
    },
    {
      id: 3,
      title: 'post c',
      author: 1
    },
    {
      id: 4,
      title: 'post d',
      author: 2
    },
    {
      id: 5,
      title: 'post e',
      author: 2
    }
  ]
};
