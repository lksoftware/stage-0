import db from '../dummyDb';

export default {
  Query: {
    users: async (root, args, context, info) => {
      return db.Users;
    }
  },
  User: {
    posts: async (user, args, context, info) => {
      return db.Posts.filter(p => p.author === user.id);
    }
  }
};
