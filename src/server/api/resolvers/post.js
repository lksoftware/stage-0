import db from '../dummyDb';

export default {
  Query: {
    posts: async (root, args, context, info) => {
      return db.Posts;
    }
  },
  Post: {
    author: async (post, args, context, info) => {
      return db.Users[post.author];
    }
  }
};
