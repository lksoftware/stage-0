import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import express from 'express';
import { makeExecutableSchema } from 'graphql-tools';

import resolvers from './resolvers';
import typeDefs from './typeDefs';

const Router = express.Router();
const schema = makeExecutableSchema({ typeDefs, resolvers });
Router.get(
  '/tool',
  graphiqlExpress({ endpointURL: 'http://localhost:3000/api' })
); // if you want GraphiQL enabled
Router.use('/', graphqlExpress({ schema }));

export default Router;
