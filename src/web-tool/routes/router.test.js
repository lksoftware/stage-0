import React from 'React';
import Router from './router';

it('should render without crash', () => {
  const wrapper = global.shallow(<Router />);
  expect(wrapper).toMatchSnapshot();
});
