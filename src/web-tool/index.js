import React from 'react';
import { render } from 'react-dom';
import Consologger from 'consologger';
import Router from './routes/router';

const App = <Router />;
render(App, document.getElementById('root'));

const logger = new Consologger();
logger.addPreset({
  name: 'blackAndWhite',
  style: {
    padding: '5px',
    'font-size': '20pt'
  }
});
logger.addPreset({
  name: 'bigRed',
  style: {
    color: 'red',
    'font-size': '35pt',
    'text-stroke': '1.0px #000000',
    '-webkit-text-stroke': '1.0px #000000',
    'font-weight': '700'
  }
});

if (process.env.NODE_ENV !== 'development') {
  logger.bigRed('Dừng lại!!!').print();
  logger
    .blackAndWhite(
      'Đây là một tính năng của trình duyệt dành cho lập trình viên của Vietface. Nếu ai đó bảo bạn sao chép-dán nội dung nào đó vào đây để bật một tính năng của Vietface hoặc "hack" tài khoản của người khác, thì đó là hành vi lừa đảo.'
    )
    .print();
} else logger.blackAndWhite('✅Application started!!!').print();
